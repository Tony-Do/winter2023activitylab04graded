public class Microwave
{
	private int Size;
	private String Colour;
	private double Cost;
	
	public Microwave(int Size, String Colour, double Cost)
	{
		this.Size = Size;
		this.Colour = Colour;
		this.Cost = Cost;
	}
	
	public void printSize()
	{
		System.out.println("The size of the microwave is around " + Size);
		
	}
	public void printCost()
	{	
		System.out.println("The cost of this microwave is " + Cost);	
		
	}
	public void printNewCost(int age) // instance method added // 
	{
		if(age >= 10 )
		{
			this.Cost = 55.5; // Assuming it is in dollars //  
			System.out.println("The new Cost of this microwave is " + this.Cost);
		}
		
	}
	private String preciseColour() // Helper method // 
	{
			return "This is a helper method to verify if the output of the colour is right: " + Colour;
		
	}
	public String getpreciseColour() // getMethod for the helper Method //
	{
		return "This is a helper method to verify if the output of the colour is right: " + this.Colour;

	}
	public void setpreciseColour(String newColour) // Setter for the helper method // 
	{
		this.Colour = newColour;
	}


	public int getSize()
	{
		return this.Size;
	}
	/*public void setSize(int newSize)
	{
		this.Size = newSize;
	}*/
	public String getColour()
	{
		return this.Colour;
	}
	public void setColour(String newColour)
	{
		this.Colour = newColour;
	}
	public double getCost()
	{
		return this.Cost;
	}
	public void setCost(double newCost)
	{
		this.Cost = newCost;
	}
	
}
