import java.util.Scanner;
public class ApplianceStore
{
	public static void main(String[]args)
	{
		Scanner scan = new Scanner(System.in);
		
		Microwave[] microwave = new Microwave[4];
		
		for(int i = 0; i < microwave.length; i++)
		{
			int size = scan.nextInt(); // Since every field is now private, you gotta set the Size //

			String colour = scan.next();	// Setting the value of it //
						  
			double cost = scan.nextDouble(); // Setting the value of it //

			microwave[i] = new Microwave(size,colour,cost);
			
		}
		
		System.out.println("Enter a new size: "); // ask a new size // 
		int newSize = scan.nextInt();

		System.out.println("Enter a new colour: "); // ask a new colour //
		String newColour = scan.next();

		System.out.println("Enter a cost you wish it was instead: "); // ask a new cost // 
		double newCost = scan.nextDouble();

		System.out.println("This is the result before calling setter methods for the last appliance: " + microwave[3].getSize() + microwave[3].getColour() + microwave[3].getCost()); 

		microwave[3].setColour(newColour); // updating values //
		microwave[3].setCost(newCost);		// size is not there because we were asked to delete 1 setter method // 

		System.out.println("This is the result after calling setter methods for the last appliance: " + microwave[3].getSize() + microwave[3].getColour() + microwave[3].getCost());


		// basically, before setting the new values, it will take the original values, after setting the new values, of course, the results are different // 


		System.out.println("The size of it is " + microwave[3].getSize()); // Get the value of it // 

		System.out.println(microwave[0].getpreciseColour()); // Adding helper method // 
		// Should output the colour of the first appliance // 

		System.out.println("The cost of it is " + microwave[3].getCost()); // Get the value of it // 

		//microwave[0].printSize();// This can be ignored
		//microwave[0].printCost();// 
		
		microwave[1].printNewCost(15); 
		// Recalling the new instance method printNewCost // -- Any inputs will result in making the cost: 55.5. Only for the second object.
		scan.close();
	}
	
	
	
}